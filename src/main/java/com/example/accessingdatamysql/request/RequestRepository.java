package com.example.accessingdatamysql.request;

import org.springframework.data.repository.CrudRepository;
public interface RequestRepository extends CrudRepository<Request, Integer>{
}
